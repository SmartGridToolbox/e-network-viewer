# e-network-viewer: JavaScript viewer for electricity networks.

e-network-viewer is a reusable Javascript viewer for electricity networks. It is designed to be incorporated into a complete project - for example, see the [SmartGridToolbox-based web-app viewer](https://gitlab.com/SmartGridToolbox/SmartGridToolbox/-/tree/master/extras/SgtClient) or the stand-alone [sgt-viewer](https://gitlab.com/SmartGridToolbox/sgt-viewer) written in electron.
