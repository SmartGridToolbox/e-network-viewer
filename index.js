// Copyright 2020 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// e-network-viewer was written by Dan Gordon.

// TODO: for await of.

"use strict";

const createGraph = require("ngraph.graph");
const createLayout = require("ngraph.forcelayout");
const createPixiGraphics = require("ngraph.pixi");
const $ = window.jQuery = require("jquery"); // Needed for flexijsoneditor below.
const fje = require("FlexiJsonEditor");
const heatmap = require("dexter-heatmap").DexterHeatmap;
const L = require("leaflet");
const pixi = require("pixi.js");

// Parameters:
const params = {
    nomEdgeLength: 60.0,
    txSpringL: 10.0,
    connSeparator: 2,
    txSeparator: 6,
    nodeViews: {
        bus: [{r: 9, c: 0x0066ff}, {r: 12, c: 0xff0000}],
        load: [{r: 4, c: 0xa009999}, {r: 6, c: 0xff0000}],
        gen: [{r: 15, c: 0x00ff00}, {r: 18, c: 0xff0000}],
    },
    linkViews: {
        "line": [{isVis: true, w: 4, c: 0x505050}, {isVis: true, w: 5, c: 0xff0000}],
        "transformer": [{isVis: true, w: 8, c: 0x2ecc71}, {isVis: true, w: 10, c: 0xff0000}],
        "connector": [{isVis: true, w: 1, c: 0x909090}, {isVis: true, width: 1, c: 0xff0000}]
    },
    // mapUrl: "https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}",
    mapUrl: "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
    mapToken: "pk.eyJ1IjoiZGV4dGVydXJiYW5lIiwiYSI6ImNsZ2JucmVmczBtZHMzaG83bTJieDFudHkifQ.La6yr7aEnsCXauv5m-Wr2A",
    sortKeys: ["id", "phs", "cons", "con", "branchType", "isInService", "vBase", "vRmsPu", "pTot", "xy", "properties"],
    colours: [
        [1.0, 0x0066cc],
        [100.0, 0x00b359],
        [1000000.0, 0x7300e6],
    ],
};

function pickColor(vBase) {
    for (let i = 0; i < params.colours.length; ++i) {
        const c = params.colours[i]
        if (vBase < c[0]) {
            return c[1]; 
        }
    }
    return params.colours[params.colours.length - 1][1]; 
}

let busViews = {}
    
class Ui {
    static graphics = null;
    static scale = 1.0;
    static pow10PScale = 1.0;

    // TODO: where does this belong?
    static _circleTexture = null;
    static circleTexture() {
        if (Ui._circleTexture == null) {
            Ui._circleTexture = Ui.graphics.makeCircleTexture(Ui.graphics.app.renderer);
        }
        return Ui._circleTexture;
    }
    
    _views = null;
    _viewIdx = 0;

    constructor(views) {
        this._views = Array.from(views);
    }

    set viewIdx(viewIdx) {
        this._viewIdx = viewIdx;
        this._viewIdxUpdated();
    }

    view() {
        return this._views[this._viewIdx];
    }

    render(ctx) {}
    rescale() {}

    _viewIdxUpdated() {}
}

class NodeUi extends Ui {
    
    _nodeSprite = null;
    
    constructor(views) {
        super(views);
        const view = this.view()
        this._nodeSprite = Ui.graphics.makeCircleSprite(Ui.circleTexture(Ui.graphics), 2.0 * view.r, view.c);
        Ui.graphics.nodeContainer.addChild(this._nodeSprite);
    }

    render(ctx) {
        this._nodeSprite.x = this.pos.x;
        this._nodeSprite.y = this.pos.y;
    }

    rescale() {
        this._updateR();
    }

    _viewIdxUpdated() {
        this._updateR();
        this._updateC();
    }
    
    get r() {
        return 0.5 * this._nodeSprite.width;
    }

    // I think needed by PixiGraph getNodeAt?
    get width() {
        return this._nodeSprite.width;
    }
    
    // I think needed by PixiGraph getNodeAt?
    get height() {
        return this._nodeSprite.height;
    }
    
    _updateR() {
        let r = Ui.scale * this.view().r;
        this._nodeSprite.width = 2.0 * r;
        this._nodeSprite.height = 2.0 * r;
        this._nodeSprite.x = this._nodeSprite.x;
    }

    _updateC() {
        let c = this.view().c;
        this._nodeSprite.tint = c;
    }
}

class LoadUi extends NodeUi {
    
    _loadSprite = null;
    _p = 0.0;

    constructor(views) {
        super(views);
        this._loadSprite = Ui.graphics.makeCircleSprite(Ui.circleTexture(Ui.graphics), 0.0, 0x000000);
        this._loadSprite.alpha = 0.25;
        Ui.graphics.loadContainer.addChild(this._loadSprite);
    }
    
    render(ctx) {
        super.render(ctx);
        this._loadSprite.x = this.pos.x;
        this._loadSprite.y = this.pos.y;
    }
    
    rescale() {
        super.rescale();
        this._updateLoadR();
    } 

    set p(p) {
        this._p = p;
        this._updateLoadR();
    }

    _updateLoadR() {
        const r = 0.55 * this._nodeSprite.width + Ui.scale * Math.sqrt(Math.abs(this._p) * Ui.pow10PScale);
        this._loadSprite.width = 2.0 * r;
        this._loadSprite.height = 2.0 * r;
        const c = this._p < 0.0 ? 0xff0000 : 0x0000ff;
        this._loadSprite.tint = c;
    }
}

function createNodeUi(node) {
    let ui = null;
    if (node.data.nodeType == "gen" || node.data.nodeType == "load") {
        ui = new LoadUi(params.nodeViews[node.data.nodeType]);
    } else if (node.data.nodeType == "bus") { 
        ui = new NodeUi(busViews[node.data.userData.vBase]);
    } else {
        ui = new NodeUi(params.nodeViews["other"]);
    }
    node.ui = ui;
    return node.ui;
}

class LinkUi extends Ui {
    
    constructor(views, link) {
        super(views);
        this._link = link;
    }
    
    get width() {
        return this.view().w;
    }
   
    render(ctx) {
        const view = this.view();
        const a = (this._link.data.linkType == "connector" || this._link.data.userData.isInService) ? 1.0 : 0.2;
        ctx.lineStyle(view.w * Ui.scale, view.c, a);
        ctx.moveTo(this.from.x, this.from.y);
        ctx.lineTo(this.to.x, this.to.y);
    }
}

function createLinkUi(link) {
    let ui = null;
    const tp = link.data?.userData?.branchType ?? link.data.linkType;
    ui = new LinkUi(params.linkViews[tp], link);
    link.ui = ui;
    return link.ui;
}

function renderUi(obj, ctx) {
    obj.render(ctx);
}

class NetworkGraph {
    nodeDict = {};
    branchDict = {};
    nodes = [];
    buses = [];
    gens = [];
    loads = [];
    branches = [];
    lines = [];
    transformers = [];
    connectors = [];

    constructor(netwJsn) {
        this.graph = createGraph();

        for (let bus of netwJsn.buses) {
            let ll = 'latlong' in bus ? bus.latlong : {lat: 0.0, long: 0.0};
            ll.lat += randOff();
            ll.long += randOff();
            let nd = this.graph.addNode(
                bus.id, 
                {
                    nodeType: "bus",
                    isSelected: false,
                    latlong: ll,
                    pos: {x: 0.0, y: 0.0},
                    gens: [],
                    loads: [],
                    userData: bus
                }
            );
            this.nodeDict[bus.id] = nd;
            this.nodes.push(nd);
            this.buses.push(nd);
        }

        for (let load of netwJsn.loads) {
            const bus = this.nodeDict[load.con.bus];

            let nd = this.graph.addNode(
                load.id,
                {
                    nodeType: "load",
                    isSelected: false,
                    pos: bus.data.pos,
                    bus: bus,
                    userData: load
                }
            );
            this.nodeDict[load.id] = nd;
            this.nodes.push(nd);
            this.loads.push(nd);
            bus.data.loads.push(nd);

            let link = this.graph.addLink(
                bus.id, load.id,
                {
                    linkType: "connector",
                    isSelected: false,
                }
            );
            this.connectors.push(link);
        }

        for (let gen of netwJsn.gens) {
            const bus = this.nodeDict[gen.con.bus];

            let nd = this.graph.addNode(
                gen.id,
                {
                    nodeType: "gen",
                    isSelected: false,
                    pos: bus.data.pos,
                    bus: bus,
                    userData: gen
                }
            );
            this.nodeDict[gen.id] = nd;
            this.nodes.push(nd);
            this.gens.push(nd);
            bus.data.gens.push(nd);

            let link = this.graph.addLink(bus.id, gen.id, {
                linkType: "connector",
                isSelected: false,
            });
            this.connectors.push(link);
        }

        for (let branch of netwJsn.branches) {
            const bus0 = this.nodeDict[branch.cons[0].bus];
            const bus1 = this.nodeDict[branch.cons[1].bus];

            const link = this.graph.addLink(branch.cons[0].bus, branch.cons[1].bus, {
                linkType: branch.branchType,
                isSelected: false,
                bus0: bus0,
                bus1: bus1,
                userData: branch
            });

            this.branchDict[branch.id] = link;
            this.branches.push(link);
            let list = branch.branchType == "line" ? this.lines : this.transformers;
            list.push(link);

            const useL = false;
            if (useL && "length" in branch.data.properties) {
                link.length = branch.properties.length;
            }
            else if (link.data.linkType == "transformer") {
                link.length = params.txSpringL;
            }
            else {
                link.length = params.nomEdgeLength;
            }
        }

        this.graph.on("changed", (e) => {});
    }
}

class MapTransform {
    constructor(lat0, long0) {
        const rEarth = 6378137.0;
        const s = rEarth * Math.PI / 180.0;
        this.lat0 = lat0;
        this.long0 = long0;
        this.fLongToX = s * Math.cos(this.lat0 * Math.PI / 180.0); // Multiply x to get long
        this.fLatToY = -s; // Multiply y to get lattitude 
        this.fXToLong = 1.0 / this.fLongToX;
        this.fYToLat = 1.0 / this.fLatToY;
    }

    xyToLatLong(xy) {

        return {
            lat: xy.y * this.fYToLat + this.lat0,
            lon: xy.x * this.fXToLong + this.long0
        };
    }

    latLongToXy(latlong) {
        return {
            x: (latlong.long - this.long0) * this.fLongToX,
            y: (latlong.lat - this.lat0) * this.fLatToY
        };
    }
}

class Map {
    static _map = null;

    _viewport = null;
    _div = null;
    _mapTransform = null;
    _hidden = true;

    constructor(viewport, div, mapTransform) {
        this._viewport = viewport;
        this._div = div;
        this._mapTransform = mapTransform;

        if (Map._map == null) Map._map = L.map("map", {zoomControl: false, zoomSnap: 0});
        L.tileLayer(params.mapUrl, {
            attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
            maxZoom: 25,
            id: "mapbox/streets-v11",
            tileSize: 512,
            zoomOffset: -1,
            accessToken: params.mapToken
        }).addTo(Map._map);
    }

    hide() {
        this._hidden = true;
        this._div.setAttribute("hidden", true);
    }

    unhide() {
        this._hidden = false;
        this._div.removeAttribute("hidden");
        this.refresh();
    }

    toggleHide() {
        if (this._hidden) {
            unhide()
        } else {
            hide()
        };
    }
    
    refresh() {
        if (this._hidden) return;
        const p0 = {
            x: this._viewport.left,
            y: this._viewport.top
        };
        const p1 = {
            x: this._viewport.right,
            y: this._viewport.bottom
        };

        const ll0 = this._mapTransform.xyToLatLong(p0);
        const ll1 = this._mapTransform.xyToLatLong(p1);

        Map._map.fitBounds([[ll0.lat, ll0.lon], [ll1.lat, ll1.lon]], {animate: false});
        Map._map.invalidateSize();
    }
}

class Dom {
    layersDiv = null;
    heatmapDiv = null;
    heatmapCanvas = null;
    mapDiv = null;
    networkDiv = null;
    networkCanvas = null;
    propertiesDiv = null;

    constructor(networkContainer, propertiesContainer) { 
        // <div class="dexter-panel dexter-layers" style="height: 100%;">
        this.layersDiv = document.createElement("div");
        this.layersDiv.id = "network-panel";
        this.layersDiv.className = "dexter-panel dexter-layers";
        this.layersDiv.style.height = "100%";
        networkContainer.appendChild(this.layersDiv);

        // <div id="heatmap" style="width: 100%; height: 100%; position:absolute; z-index: 10">
        this.heatmapDiv = document.createElement("div");
        this.heatmapDiv.id = "heatmap-div";
        this.heatmapDiv.style.width = "100%";
        this.heatmapDiv.style.height = "100%";
        this.heatmapDiv.style.position = "absolute";
        this.heatmapDiv.style.zIndex = "20";
        this.heatmapDiv.style.opacity = "0.7";
        this.layersDiv.appendChild(this.heatmapDiv);

        // <div id="map" hidden style="width: 100%; height: 100%; position:absolute; z-index: 20; opacity: 0.6"></div>
        this.mapDiv = document.createElement("div");
        this.mapDiv.id = "map";
        this.mapDiv.hidden = true;
        this.mapDiv.style.width = "100%";
        this.mapDiv.style.height = "100%";
        this.mapDiv.style.position = "absolute";
        this.mapDiv.style.zIndex = "10";
        this.mapDiv.style.opacity = "0.9";
        this.layersDiv.appendChild(this.mapDiv);

        // <div id="network" style="width: 100%; height: 100%; position:absolute; z-index: 30">
        this.networkDiv = document.createElement("div");
        this.networkDiv.id = "network-div";
        this.networkDiv.style.width = "100%";
        this.networkDiv.style.height = "100%";
        this.networkDiv.style.position = "absolute";
        this.networkDiv.style.zIndex = "30";
        this.layersDiv.appendChild(this.networkDiv);

        // <canvas class="dexter-layer" id="network-canvas" style="background-color: rgba(255, 255, 255, 0.0);"></canvas>
        this.networkCanvas = document.createElement("canvas");
        this.networkCanvas.className = "dexter-layer";
        this.networkCanvas.style.backgroundColor = [255, 255, 255, 0.0];
        this.networkDiv.appendChild(this.networkCanvas);

        // <div class="dexter-panel dexter-network-properties json-editor" id="network-properties" style="height: 100%; font-size: 12px;"></div>
        this.propertiesDiv = document.createElement("div");
        this.propertiesDiv.className = "dexter-panel dexter-network-properties json-editor";
        this.propertiesDiv.style = "height: 100%; font-size: 12px";
        propertiesContainer.appendChild(this.propertiesDiv);
    }
}

class Viewer {
    _dom = null;

    _netwGraph = null;
    _layout = null;
    _graphics = null;
    _map = null;

    _selNodes = new Set();
    _selBranches = new Set();

    _useSprings = false;
    _pinNodes = false;
    _showHeatmap = false;
    _showMap = false;
    _vLow = 0.0;
    _vHigh = 0.0;
    _springCoeff = 0.0;
    _repulsion = 0.0;
    _drag = 0.0;

    _propertiesUpdateCb = (compId, compType) => {
        if (compId in this._netwGraph.nodeDict) {
            return this._netwGraph.nodeDict[compId].data.userData;
        } else if (compId in this._netwGraph.branchDict) {
            return this._netwGraph.branchDict[compId].data.userData;
        } else {
            return null;
        }
    };

    constructor(networkContainer, propertiesContainer) {
        this._dom = new Dom(networkContainer, propertiesContainer);
        this._dom.heatmapCanvas = heatmap.init(this._dom.heatmapDiv);
        function rh () {this.refreshHeatmap();}
        this.resizeHandler = rh.bind(this);
        window.addEventListener("resize", this.resizeHandler);
    }

    calcCentre() {
        let maxLong = -1e10;
        let minLong = 1e10;
        let maxLat = -1e10;
        let minLat = 1e10;
        for (const nd of this._netwGraph.buses) {
            if (nd.data.latlong.lat > maxLat) maxLat = nd.data.latlong.lat;
            if (nd.data.latlong.lat < minLat) minLat = nd.data.latlong.lat;
            if (nd.data.latlong.long > maxLong) maxLong = nd.data.latlong.long;
            if (nd.data.latlong.long < minLong) minLong = nd.data.latlong.long;
        }
        const lat0 = 0.5 * (minLat + maxLat);
        const long0 = 0.5 * (minLong + maxLong);
        return {lat: lat0, long: long0}; 
    }

    loadNetwork(netwJsn) {
        this._netwGraph = new NetworkGraph(netwJsn);
        const centre0 = this.calcCentre();
        this._mapTransform = new MapTransform(centre0.lat, centre0.long);

        // Create the layout.
        {
            // Set node pos based on bus coords and transform:
            for (let nd of this._netwGraph.buses) {
                nd.data.pos = this._mapTransform.latLongToXy(nd.data.latlong);
                nd.data.canPin = true;

                for (let load of nd.data.loads) {
                    load.data.pos = nd.data.pos;
                }

                for (let gen of nd.data.gens) {
                    gen.data.pos = nd.data.pos;
                }
            }

            const forceParams = {
                timeStep: 10,
                theta: 0.8,
                springLength : params.nomEdgeLength,
                springCoefficient : 0.0005,
                dragCoefficient : 0.05,
                gravity : -0.01
            };

            this._layout = createLayout(this._netwGraph.graph, forceParams);

            // Locate and pin buses in layout.
            for (const node of this._netwGraph.nodes)
            {
                this._layout.setNodePosition(node.id, node.data.pos.x, node.data.pos.y);
                if (node.data.nodeType == "bus" && node.data.canPin) {
                    this._layout.pinNode(node, true);
                }
            }
        }

        // Populate busViews.
        {
            for (const bus of this._netwGraph.buses) {
                if (!(bus.data.userData.vBase in busViews)) {
                    busViews[bus.data.userData.vBase] = structuredClone(params.nodeViews['bus']);
                }
            }
            let i = 0;
            for (const bv in busViews) {
                busViews[bv][0].c = pickColor(bv);
            }
        }

        // Create graphics.
        {
            const opts = {
                container: this._dom.networkDiv,
                view: this._dom.networkCanvas,
                layout: this._layout
            };

            this._graphics = createPixiGraphics(this._netwGraph.graph, opts);
            Ui.graphics = this._graphics;

            this._graphics.loadContainer = new pixi.Container();
            this._graphics.viewport.addChild(this._graphics.loadContainer);
            this._graphics.linkGraphics.zIndex = 0;
            this._graphics.loadContainer.zIndex = 1;
            this._graphics.nodeContainer.zIndex = 2;
            this._graphics.viewport.children.sort((a, b) => a.zIndex - b.zIndex);
            this._graphics.createNodeUI(createNodeUi);
            this._graphics.renderNode(renderUi);
            this._graphics.createLinkUI(createLinkUi);
            this._graphics.renderLink(renderUi);
            this._graphics.app.view.onwheel = (e) => {e.preventDefault();};

            this._graphics.viewport.on("zoomed", (e) => {if (this._showHeatmap) this.hideHeatmap();});
            this._graphics.viewport.on("zoomed-end", (e) => {if (this._showHeatmap) this.unhideHeatmap();});
            this._graphics.viewport.on("mousedown", (e) => {if (this._showHeatmap) this.hideHeatmap();});
            this._graphics.viewport.on("mouseup", (e) => {if (this._showHeatmap) this.unhideHeatmap();});
            // this._graphics.viewport.on("click", (e) => {this.clearSel();});

            this._graphics.viewport.on("zoomed", (e) => {this._map?.refresh();});
            this._graphics.viewport.on("moved", (e) => {this._map?.refresh();});

            this._graphics.onNodeClicked = node => {
                const isSel = node.data.isSelected;
                this.clearSel();
                if (!isSel) {
                    this.selectNode(node.id);
                }
            }

            this._graphics.onLinkClicked = link => {
                if (link.data.linkType != "connector") {
                    const isSel = link.data.isSelected;
                    this.clearSel();
                    if (!isSel) {
                        this.selectBranch(link.data.userData.id);
                    }
                }
            }

            const oldRenderOne = this._graphics.renderOneFrame;
            this._graphics.renderOneFrame = () => {
                if (this._showHeatmap) this.drawHeatmap();
                oldRenderOne();
            };
        }
        
        this._map = new Map(this._graphics.viewport, this._dom.mapDiv, this._mapTransform);

        this.initSettings();

        this.updateLinkLengths();

        const pinNodesSv = this._pinNodes;
        this.setPinNodes(true);
        for (let i = 0; i < 100; ++i) {
            this._layout.step();
        }
        this.setPinNodes(pinNodesSv);

        this._graphics.renderOneFrame();
    }

    // -------------------------------------------------------------------------
    // Settings
    // -------------------------------------------------------------------------

    setUseSprings(val) {this._useSprings = val; this.updateUseSprings();}
    updateUseSprings() {
        if (this._useSprings) {
            this._graphics?.start();
        } else {
            this._graphics?.stop();
        }
    }

    setPinNodes(val) {this._pinNodes = val; this.updatePinNodes();}
    updatePinNodes() {
        if (this._netwGraph != null) {
            if (this._pinNodes) {
                for (const node of this._netwGraph.buses) {
                    if (node.data.canPin) {
                        this._layout.pinNode(node, true);
                    }
                };
            } else {
                this._netwGraph.graph.forEachNode(node => {this._layout.pinNode(node, false);});
            }
        }
    }

    setShowHeatmap(val) {this._showHeatmap = val; this.updateShowHeatmap()}
    updateShowHeatmap() {
        if (this._showHeatmap) {
            this.unhideHeatmap();
        }
        else {
            this.hideHeatmap();
        }
    }
    hideHeatmap() {
        this._dom.heatmapDiv.setAttribute("hidden", true);
    }
    unhideHeatmap() {
        this._dom.heatmapDiv.removeAttribute("hidden");
        this.refreshHeatmap()
    }

    setShowMap(val) {this._showMap = val; this.updateShowMap();}
    updateShowMap() {
        if (this._showMap) {
            this.unhideMap();
        } else {
            this.hideMap();
        }
    }
    hideMap() {
        this._map?.hide();
    }
    unhideMap() {
        this._map?.unhide();
    }

    setVLow(val) {this._vLow = val; this.updateVLow();}
    updateVLow() {
        if (this._showHeatmap) this.drawHeatmap();
    }

    setVHigh(val) {this._vHigh = val; this.updateVHigh();}
    updateVHigh() {
        if (this._showHeatmap) this.drawHeatmap();
    }

    setPScale(val) {Ui.pow10PScale = Math.pow(10, val); this.updatePScale();}
    updatePScale() {
        if (this._netwGraph != null)
        {
            for (let i = 0; i < this._netwGraph.loads.length; ++i) {
                const node = this._netwGraph.graph.getNode(this._netwGraph.loads[i].id);
                node.ui.p = node.data.userData.pTot;
            }
            for (let i = 0; i < this._netwGraph.gens.length; ++i) {
                const node = this._netwGraph.graph.getNode(this._netwGraph.gens[i].id);
                node.ui.p = -node.data.userData.pTot;
            }
        }
    }

    setSpringCoeff(val) {this._springCoeff = val; this.updateSpringCoeff();}
    updateSpringCoeff() {
        if (this._layout != null) {
            this._layout.simulator.settings.springCoefficient = this._springCoeff;
        }
    }

    setRepulsion(val) {this._repulsion = val; this.updateRepulsion();}
    updateRepulsion() {
        if (this._layout != null) {
            this._layout.simulator.gravity(this._repulsion);
        }
    }

    setDrag(val) {this._drag = val; this.updateDrag();}
    updateDrag() {
        if (this._layout != null) {
            this._layout.simulator.settings.dragCoefficient = this._drag;
        }
    }

    setScale(val) {
        Ui.scale = val;
        if (this._netwGraph != null) {
            this._netwGraph.graph.forEachNode((nd) => {nd.ui.rescale();});
            this.updateLinkLengths();
            this._graphics.renderOneFrame();
        }
    }

    updateLinkLengths() {
        for (const link of this._netwGraph.connectors) {
            const nd0 = this._netwGraph.graph.getNode(link.fromId);
            const nd1 = this._netwGraph.graph.getNode(link.toId);
            const spr = this._layout.getSpring(link.fromId, link.toId);
            spr.length = link.length = nd0.ui.r + nd1.ui.r + params.connSeparator;
        }
        for (const link of this._netwGraph.transformers) {
            const nd0 = this._netwGraph.graph.getNode(link.fromId);
            const nd1 = this._netwGraph.graph.getNode(link.toId);
            const spr = this._layout.getSpring(link.fromId, link.toId);
            spr.length = link.length = nd0.ui.r + nd1.ui.r + params.txSeparator;
        }
    }

    initSettings() {
        this.updateUseSprings();
        this.updatePinNodes();
        this.updateShowHeatmap();
        this.updateShowMap();
        this.updateVLow();
        this.updateVHigh();
        this.updatePScale();
        this.updateSpringCoeff();
        this.updateRepulsion();
        this.updateDrag();
    }

    refreshHeatmap() {
        if (this._showHeatmap && this._graphics != null && this._graphics.viewport != null) {
            heatmap.setViewRect(
                this._graphics.viewport.left, this._graphics.viewport.top, 
                this._graphics.viewport.right, this._graphics.viewport.bottom);
            this.drawHeatmap();
        }
    }

    drawHeatmap() {
        if (!this._netwGraph) return;
        const dat = [];
        const vRange = this._vHigh - this._vLow;
        for (const node of this._netwGraph.buses) {
            const pos = this._layout.getNodePosition(node.id);
            const VParam = (node.data.userData.vRmsPu - this._vLow) / vRange;
            dat.push([[pos.x, pos.y], VParam]);
        };
        heatmap.setData(dat);
        heatmap.draw(); // adds the buffered points
    }

    search(regexStr) {
        if (this._netwGraph != null) {
            this.clearSel();
            re = new RegExp(regexStr);
            for (let id of Object.keys(this._netwGraph.nodeDict)) {
                if (re.test(id)) {
                    this.selectNode(id);
                }
            }
            for (let id of Object.keys(this._netwGraph.branchDict)) {
                if (re.test(id)) {
                    this.selectBranch(id);
                }
            }
        }
    }

    centerSel() {
        for (let id of this._selNodes) {
            const pos = this._layout.getNodePosition(id);
            this._graphics.viewport.moveCenter(pos.x, pos.y);
            return;
        }
        for (let id of this._selBranches) {
            const link = this._netwGraph.branchDict[id];
            const ndFromId = link.fromId;
            const ndFrom = this._layout.getNodePosition(ndFromId);
            const ndToId = link.toId;
            const ndTo = this._layout.getNodePosition(ndToId);
            const pos = {x: 0.5*(ndFrom.x + ndTo.x), y: 0.5*(ndFrom.y + ndTo.y)};
            this._graphics.viewport.moveCenter(pos.x, pos.y);
            return;
        }
    }

    refreshNetworkState(netwStateJsn) {
        for (let i = 0; i < netwStateJsn.busVoltages.length; ++i) {
            this._netwGraph.buses[i].data.userData.vRmsPu = netwStateJsn.busVoltages[i];
        }
        for (let i = 0; i < netwStateJsn.genPowers.length; ++i) {
            let node = this._netwGraph.gens[i]
            node.data.userData.pTot = netwStateJsn.genPowers[i];
            node.ui.p = -node.data.userData.pTot;
        }
        for (let i = 0; i < netwStateJsn.loadPowers.length; ++i) {
            let node = this._netwGraph.loads[i]
            node.data.userData.pTot = netwStateJsn.loadPowers[i];
            node.ui.p = node.data.userData.pTot;
        }
        this._graphics.renderOneFrame();
    }

    setPropertiesUpdate(f) {
        this._propertiesUpdateCb = f;
    }

    loadEJson(eJson) {
        this.loadNetwork(convertEJson(eJson));
    }

    async updateJsonEditor(userData, view, opt) {
        let updated = await this._propertiesUpdateCb(userData.id, view);
        opt.keys = Viewer.sortedKeys(updated, params.sortKeys);
        $(this._dom.propertiesDiv).jsonEditor(updated, opt);
    }

    async selectNode(id) {
        const node = this._netwGraph.graph.getNode(id);
        if (typeof node === "undefined") return
        node.data.isSelected = true;
        this._selNodes.add(id);
        node.ui.viewIdx = 1;

        const opt = { 
            change: data => {},
            propertyclick: path => {},
            replacer: Viewer.replacer
        };

        const view = node.data.nodeType;
        this.updateJsonEditor(node.data.userData, view, opt);
        this._graphics.renderOneFrame();
    }

    async unselectNode(id) {
        const node = this._netwGraph.graph.getNode(id);
        if (typeof node === "undefined") return
        node.data.isSelected = false;
        this._selNodes.delete(id);
        node.ui.viewIdx = 0;

        $(this._dom.propertiesDiv).jsonEditor(null);
        this._graphics.renderOneFrame();
    }

    async selectBranch(id) {
        const link = this._netwGraph.branchDict[id];
        if (typeof link === "undefined") return;
        link.data.isSelected = true;
        this._selBranches.add(id);
        link.ui.viewIdx = 1;

        const opt = { 
            change: function(data) {
                const isInServ = data.userData.isInService;
                if (isInServ != link.data.userData.isInService) {
                    toggle(link);
                }
                return data;
            },
            propertyclick: function(path) {},
            replacer: Viewer.replacer
        };

        this.updateJsonEditor(link.data.userData, "branch", opt);
        this._graphics.renderOneFrame();
    }

    async unselectBranch(id) {
        const link = this._netwGraph.branchDict[id];
        if (typeof link === "undefined") return;
        link.data.isSelected = false;
        this._selBranches.delete(id);
        link.ui.viewIdx = 0;
        $(this._dom.propertiesDiv).jsonEditor(null);
        this._graphics.renderOneFrame();
    }

    async toggle(userData) {
        userData.data.userData.isInService = !userData.data.userData.isInService;
        this._graphics.renderOneFrame();
    }

    async clearSel() {
        for (let id of this._selNodes) {
            this.unselectNode(id);
        }
        for (let id of this._selBranches) {
            this.unselectBranch(id);
        }
    }

    static replacer(key, val) {
        return val && val.toPrecision ? Number(val.toPrecision(6)) : val;
    }

    static sortedKeys(json, keys) {
        let unsortedKeys = [];
        for (let key in json) {
            if (json.hasOwnProperty(key)) {
                if (keys.indexOf(key) == -1) {
                    unsortedKeys.push(key);
                }
            }
        }

        let result = [];

        for (let i = 0; i < keys.length; ++i) {
            result.push(keys[i]);
        }

        for (let i = 0; i < unsortedKeys.length; ++i) {
            result.push(unsortedKeys[i]);
        }

        return result;
    }
}

const ENetworkViewer = function(networkContainer, propertiesContainer) {
    const _viewer = new Viewer(networkContainer, propertiesContainer);
    return {
        setUseSprings: val => _viewer.setUseSprings(val),
        setPinNodes: val => _viewer.setPinNodes(val),
        setShowHeatmap: val => _viewer.setShowHeatmap(val),
        setShowMap: val => _viewer.setShowMap(val),
        setVLow: val => _viewer.setVLow(val),
        setVHigh: val => _viewer.setVHigh(val),
        setPScale: val => _viewer.setPScale(val),
        setSpringCoeff: val => _viewer.setSpringCoeff(val),
        setRepulsion: val => _viewer.setRepulsion(val),
        setDrag: val => _viewer.setDrag(val),
        search: val => _viewer.search(val),
        centerSel: () => _viewer.centerSel(),
        loadNetwork: val => _viewer.loadNetwork(val),
        loadEJson: val => _viewer.loadEJson(val),
        refreshNetworkState: val => _viewer.refreshNetworkState(val),
        setPropertiesUpdate: val => _viewer.setPropertiesUpdate(val),
        setScale: val => _viewer.setScale(val)
    };
};

module.exports = {params: params, ENetworkViewer: ENetworkViewer};

function convertEJson(inJsn) {

    function isInServ(obj) {
        if ("in_service" in obj && obj.in_service == false) return false;
        for (let con of obj.cons) if ("closed" in con && con.closed == false) return false;
        return true;
    }

    function vRmsPu(obj, vType) {
        let retval = 1.0;
        function mag2(x) {
            return Math.pow(x[0], 2) + Math.pow(x[1], 2);
        }
        function lMag2(l) {
            return l.map(mag2);
        }
        if ("v" in obj) {
            retval = Math.sqrt(mean(lMag2(obj.v))) / obj.v_base;
            if (vType == "ll") retval *= Math.sqrt(3.0)
        }
        return retval;
    }

    function pTot(obj) {
        let retval = 0.0;
        if ("s" in obj) {
            retval = obj["s"].map((x) => x[0]).reduce((a, b) => a + b, 0);
        }
        return retval;
    }

    const vType = inJsn.voltage_type;

    const newNetw = {
        buses: [],
        branches: [],
        gens: [],
        loads: [] 
    };

    for (let compId in inJsn.components) {
        const userData = inJsn.components[compId];
        const compTp = Object.keys(userData)[0];
        const obj = userData[compTp];
        let newComp = {}
        switch (compTp) {
            case "Node": {
                newComp = {
                    id: compId,
                    phs: obj["phs"],
                    vRmsPu: vRmsPu(obj, vType),
                    vBase: obj["v_base"],
                    isInService: true,
                    properties: userData
                };
                if ("latlong" in obj) {
                    newComp.latlong = obj.latlong;  
                } else {
                    newComp.latlong = {lat: 0.0, long: 0.0};
                }
                newComp.pos = {x: 0.0, y: 0.0};
                newNetw.buses.push(newComp);
                break;
            }
            case "Line":
            case "Transformer": {
                const cons = obj.cons;
                const newCons = cons.map(function(x){return {bus: x.node, phs: x.phs}});
                let branchType = "ERROR";
                if (compTp == "Line") branchType = "line";
                if (compTp == "Transformer") branchType = "transformer";
                newComp = {
                    id: compId,
                    isInService: isInServ(obj),
                    cons: newCons,
                    branchType: branchType,
                    properties: userData
                }
                newNetw.branches.push(newComp);
                break;
            }
            case "Gen":
            case "Infeeder": {
                const con = obj.cons[0];
                const newCon = {bus: con.node, phs: con.phs};
                const newComp = {
                    id: compId,
                    isInService: isInServ(obj),
                    con: {bus: obj.cons[0].node, phs: obj.cons[0].phs},
                    pTot: pTot(obj),
                    properties: userData,
                }
                newNetw.gens.push(newComp);
                break;
            }
            case "Load": {
                const con = obj.cons[0];
                const newCon = {bus: con.node, phs: con.phs};
                const newComp = {
                    id: compId,
                    isInService: isInServ(obj),
                    con: {bus: obj.cons[0].node, phs: obj.cons[0].phs},
                    pTot: pTot(obj),
                    properties: userData
                }
                newNetw.loads.push(newComp);
                break;
            }
            default:
                break;
        }
    }

    if ("map" in inJsn) newNetw.map = inJsn.map;

    return newNetw;
}

function re(a) {
    return a[0];
}

function im(a) {
    return a[1];
}

function mean(xs) {
    if (xs.length == 0) return 0; else return xs.reduce((p, c) => p + c, 0) / xs.length;
}

function median(xs) {
    if (xs.length == 0) {
        return 0;
    } else if (xs.length == 1) {
        return xs[0];
    } else {
        xs.sort(function(a, b){return a - b;});
        const middle = Math.ceil((xs.length - 1) / 2);
        return ((xs.length - 1) % 2 == 0) ? 0.5 * (xs[middle - 1] + xs[middle]) : xs[middle];
    }
}

function randOff() {
    return 2.0 * 1e-6 * (Math.random() - 0.5);
}
