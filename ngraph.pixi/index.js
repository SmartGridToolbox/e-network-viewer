var CIRCLE_RADIUS = 64; // Controls the eventual resolution of the circles.
var NODE_WIDTH = 10;
var PIXI = require('pixi.js');
var VIEWPORT = require('pixi-viewport');

var app = null;

module.exports = function (graph, settings) {
  var merge = require('ngraph.merge');

  // Initialize default settings:
  settings = merge(settings, {
    // Default physics engine settings
    physics: {
      springLength: 30,
      springCoeff: 0.0008,
      dragCoeff: 0.01,
      gravity: -1.2,
      theta: 1
    }
  });

  // Where do we render our graph?
  if (typeof settings.container === 'undefined') {
    settings.container = document.body;
  }

  // Create the PIXI application if it's not already done.
  if (app == null) {
    app = new PIXI.Application(
      {
        transparent:true,
        antialias: true,
        resizeTo: settings.container,
        view: settings.view
      });
  }
  else {
    for (var i = app.stage.children.length - 1; i >= 0; i--) {
      app.stage.removeChild(app.stage.children[i]);
    };
  }

  // If client does not need custom layout algorithm, let's create default one:
  var layout = settings.layout;

  if (!layout) {
    var createLayout = require('ngraph.forcelayout');
    var physics = require('ngraph.physics.simulator');

    layout = createLayout(graph, physics(settings.physics));
  }

  var width = settings.container.clientWidth;
  var height = settings.container.clientHeight;

  settings.container.appendChild(app.view);

  var viewport = new VIEWPORT.Viewport({
    screenWidth: app.view.width,
    screenHeight: app.view.height,
    worldWidth: 5000,
    worldHeight: 5000,
    interaction: app.renderer.plugins.interaction 
    // The interaction module is important for wheel to work properly when renderer.view is placed or scaled
  });

  app.stage.addChild(viewport);

  var linkGraphics = new PIXI.Graphics();
  viewport.addChild(linkGraphics);

  var nodeContainer = new PIXI.Container();
  viewport.addChild(nodeContainer);
  
  // activate plugins
  viewport.drag().wheel();
  viewport.center = {x: 0, y: 0};
  viewport.fitWorld(true);

  // Default callbacks to build/render nodes
  var nodeUIBuilder = defaultCreateNodeUI,
      nodeRenderer  = defaultNodeRenderer,
      linkUIBuilder = defaultCreateLinkUI,
      linkRenderer  = defaultLinkRenderer;

  // Storage for UI of nodes/links:
  var nodeUI = {}, linkUI = {};

  listenToGraphEvents();

  var isRunning = false;

  var pixiGraphics = {
    /**
     * Allows client to start animation loop, without worrying about RAF stuff.
     */
    start: start,
    
    stop: stop,

    isRunning: isRunning,

    /**
     * For more sophisticated clients we expose one frame rendering as part of
     * API. This may be useful for clients who have their own RAF pipeline.
     */
    renderOneFrame: renderOneFrame,

    /**
     * This callback creates new UI for a graph node. This becomes helpful
     * when you want to precalculate some properties, which otherwise could be
     * expensive during rendering frame.
     *
     * @callback createNodeUICallback
     * @param {object} node - graph node for which UI is required.
     * @returns {object} arbitrary object which will be later passed to renderNode
     */
    /**
     * This function allows clients to pass custom node UI creation callback
     *
     * @param {createNodeUICallback} createNodeUICallback - The callback that
     * creates new node UI
     * @returns {object} this for chaining.
     */
    createNodeUI : function (createNodeUICallback) {
      nodeUI = {};
      nodeUIBuilder = createNodeUICallback;
      graph.forEachNode(initNode);
      return this;
    },

    /**
     * This callback is called by pixiGraphics when it wants to render node on
     * a screen.
     *
     * @callback renderNodeCallback
     * @param {object} node - result of createNodeUICallback(). It contains anything
     * you'd need to render a node
     * @param {PIXI.Graphics} ctx - PIXI's rendering context.
     */
    /**
     * Allows clients to pass custom node rendering callback
     *
     * @param {renderNodeCallback} renderNodeCallback - Callback which renders
     * node.
     *
     * @returns {object} this for chaining.
     */
    renderNode: function (renderNodeCallback) {
      nodeRenderer = renderNodeCallback;
      return this;
    },

    /**
     * This callback creates new UI for a graph link. This becomes helpful
     * when you want to precalculate some properties, which otherwise could be
     * expensive during rendering frame.
     *
     * @callback createLinkUICallback
     * @param {object} link - graph link for which UI is required.
     * @returns {object} arbitrary object which will be later passed to renderNode
     */
    /**
     * This function allows clients to pass custom node UI creation callback
     *
     * @param {createLinkUICallback} createLinkUICallback - The callback that
     * creates new link UI
     * @returns {object} this for chaining.
     */
    createLinkUI : function (createLinkUICallback) {
      linkUI = {};
      linkUIBuilder = createLinkUICallback;
      graph.forEachLink(initLink);
      return this;
    },

    /**
     * This callback is called by pixiGraphics when it wants to render link on
     * a screen.
     *
     * @callback renderLinkCallback
     * @param {object} link - result of createLinkUICallback(). It contains anything
     * you'd need to render a link
     * @param {PIXI.Graphics} ctx - PIXI's rendering context.
     */
    /**
     * Allows clients to pass custom link rendering callback
     *
     * @param {renderLinkCallback} renderLinkCallback - Callback which renders
     * link.
     *
     * @returns {object} this for chaining.
     */
    renderLink: function (renderLinkCallback) {
      linkRenderer = renderLinkCallback;
      return this;
    },

    /**
     * Tries to get node at (x, y) graph coordinates. By default renderer assumes
     * width and height of the node is 10 pixels. But if your createNodeUICallback
     * returns object with `width` and `height` attributes, they are considered
     * as actual dimensions of a node
     *
     * @param {Number} x - x coordinate of a node in layout's coordinates
     * @param {Number} y - y coordinate of a node in layout's coordinates
     * @returns {Object} - actual graph node located at (x, y) coordinates.
     * If there is no node in that are `undefined` is returned.
     *
     * TODO: This should be part of layout itself
     */
    getNodeAt: getNodeAt,

    getLinkAt: getLinkAt,

    /**
     * [Read only] Current layout algorithm. If you want to pass custom layout
     * algorithm, do it via `settings` argument of ngraph.pixi.
     */
    layout: layout,

    app: app,
    viewport: viewport,
    nodeContainer: nodeContainer,
    linkGraphics: linkGraphics,

    onNodeClicked: node => {
      console.log('Node clicked ', node);
    },
    
    onLinkClicked: link => {
      console.log('Link clicked ', link);
    },

    /**
     * Helper that generates a bitmap texture containing
     * a node of the required radius.
     */
    makeCircleTexture: makeCircleTexture,
    
    /**
     * Helper that generates a circular sprite.
     */
    makeCircleSprite: makeCircleSprite
  };

  // listen to mouse events
  graphInput(pixiGraphics);

  return pixiGraphics;

///////////////////////////////////////////////////////////////////////////////
// Public API is over
///////////////////////////////////////////////////////////////////////////////

  function start() {
    isRunning = true;
    animationLoop();
  }
  
  function stop() {
    isRunning = false;
  }

  function animationLoop() {
    if (isRunning)
    {
      requestAnimationFrame(animationLoop);
      layout.step();
      pixiGraphics.renderOneFrame();
    }
  }

  function renderOneFrame() {
    linkGraphics.clear();

    Object.keys(linkUI).forEach(renderLink);

    Object.keys(nodeUI).forEach(renderNode);

    app.renderer.render(app.stage);
  }

  function renderLink(linkId) {
    linkRenderer(linkUI[linkId], linkGraphics);
  }

  function renderNode(nodeId) {
    nodeRenderer(nodeUI[nodeId]);
  }

  function initNode(node) {
    var ui = nodeUIBuilder(node);
    // augment it with position data:
    ui.pos = layout.getNodePosition(node.id);
    // and store for subsequent use:
    nodeUI[node.id] = ui;
  }

  function initLink(link) {
    var ui = linkUIBuilder(link);
    ui.from = layout.getNodePosition(link.fromId);
    ui.to = layout.getNodePosition(link.toId);
    linkUI[link.id] = ui;
  }

  function defaultCreateNodeUI() {
    if (this.nodeTexture == null) {
      this.nodeTexture = makeCircleTexture(app.renderer);
    }
    var sprite = makeCircleSprite(this.nodeTexture, NODE_WIDTH, 0xff0000);
    nodeContainer.addChild(sprite);
    return {sprite: sprite};
  }

  function defaultCreateLinkUI() {
    return {};
  }

  function defaultNodeRenderer(node) {
    node.sprite.x = node.pos.x; 
    node.sprite.y = node.pos.y;
  }

  function defaultLinkRenderer(link) {
    linkGraphics.lineStyle(1, 0xcccccc, 1);
    linkGraphics.moveTo(link.from.x, link.from.y);
    linkGraphics.lineTo(link.to.x, link.to.y);
  }

  function getNodeAt(x, y) {
    // currently it's a linear search, but nothing stops us from refactoring
    // this into spatial lookup data structure in future:
    for (var nodeId in nodeUI) {
      if (nodeUI.hasOwnProperty(nodeId)) {
        var node = nodeUI[nodeId];
        var pos = node.pos;
        var width = node.width || NODE_WIDTH;
        half = width/2;
        var insideNode = pos.x - half < x && x < pos.x + half &&
                         pos.y - half < y && y < pos.y + half;

        if (insideNode) {
          return graph.getNode(nodeId);
        }
      }
    }
  }
  
  function getLinkAt(x, y) {
    links = [];
    graph.forEachLink(link => {links.push(link);});
    for (let link of links) {
      var ui = linkUI[link.id];
      var hitRad = 0.5*ui.width + 2;
      var x1 = ui.from.x;
      var y1 = ui.from.y;
      var x2 = ui.to.x;
      var y2 = ui.to.y;

      var minX = Math.min(x1, x2)
      var maxX = Math.max(x1, x2)
      var minY = Math.min(y1, y2)
      var maxY = Math.max(y1, y2)
      if (x >= minX && x <= maxX && y >= minY && y <= maxY) {
        // Possible hit.
        dx = x2 - x1;
        dy = y2 - y1;
        dist = Math.abs(dy*x - dx*y + x2*y1 - y2*x1)/Math.sqrt(dx*dx + dy*dy);
        if (dist <= hitRad) {
          return link;
        }
      }
    };
  }

  function listenToGraphEvents() {
    graph.on('changed', onGraphChanged);
  }

  function onGraphChanged(changes) {
    for (var i = 0; i < changes.length; ++i) {
      var change = changes[i];
      if (change.changeType === 'add') {
        if (change.node) {
          initNode(change.node);
        }
        if (change.link) {
          initLink(change.link);
        }
      } else if (change.changeType === 'remove') {
        if (change.node) {
          delete nodeUI[change.node.id];
        }
        if (change.link) {
          delete linkUI[change.link.id];
        }
      }
    }
  }

  /* KLUDGE: This has been bodged up to work with pixi-viewport.
   * TODO: Do this properly!
   */
  function graphInput(graphics) {
    var getLocalPosition = PIXI.InteractionData.prototype.getLocalPosition;
    var nodeGraphics = graphics.nodeGraphics;

    addDragListener();

    var getGraphCoordinates = (function () {
      var ctx = {
        global: { x: 0, y: 0} // store it inside closure to avoid GC pressure
      };

      return function (x, y) {
        ctx.global.x = x; ctx.global.y = y;
        return getLocalPosition.call(ctx, nodeGraphics);
      };
    }());

    function addDragListener() {
      var downPos;
      var nodeUnderCursor;
      var origIsPinned;

      graphics.app.renderer.plugins.interaction.on('mousedown', e => {
        var pos = e.data.global;
        downPos = graphics.viewport.toWorld(pos.x, pos.y);
        nodeUnderCursor = graphics.getNodeAt(downPos.x, downPos.y);
        if (nodeUnderCursor) {
          origIsPinned = graphics.layout.isNodePinned(nodeUnderCursor);
          // just to make sure layouter will not attempt to move this node
          // based on physical forces. Now it's completely under our control:
          graphics.layout.pinNode(nodeUnderCursor, true);
          graphics.viewport.plugins.pause('drag');
        }
      });

      graphics.app.renderer.plugins.interaction.on('mouseup', e => {
        if (graphics.viewport.plugins.get('drag').paused) {
          graphics.viewport.plugins.resume('drag');
        }
        if (nodeUnderCursor) {
          graphics.layout.pinNode(nodeUnderCursor, origIsPinned);
        }
        nodeUnderCursor = null;
      });

      graphics.app.renderer.plugins.interaction.on('mousemove', e => {
        if (nodeUnderCursor) {
          var pos = e.data.global;
          var graphPos = graphics.viewport.toWorld(pos.x, pos.y);
          graphics.layout.setNodePosition(nodeUnderCursor.id, graphPos.x, graphPos.y);
          if (!isRunning) {
            pixiGraphics.renderOneFrame();
          }
        }
      });

      graphics.viewport.on('clicked', e => {
        if (nodeUnderCursor) {
          graphics.onNodeClicked(nodeUnderCursor);
        }
        else {
          linkUnderCursor = graphics.getLinkAt(downPos.x, downPos.y);
          if (linkUnderCursor) {
            graphics.onLinkClicked(linkUnderCursor);
          }
        }
      });
    }
  }

  function makeCircleTexture() {
    var gfx = new PIXI.Graphics();
    var tileSize = CIRCLE_RADIUS * 2;
    var texture = PIXI.RenderTexture.create(tileSize, tileSize);
    gfx.beginFill(0xffffff);
    gfx.drawCircle(tileSize / 2, tileSize / 2, CIRCLE_RADIUS);
    gfx.endFill();
    app.renderer.render(gfx, texture);
    return texture;
  }
  
  function makeCircleSprite(texture, sz, color) {
    var sprite = new PIXI.Sprite(texture);
    sprite.anchor.set(0.5);
    sprite.scale.set(0.5 * sz / CIRCLE_RADIUS);
    sprite.tint = color;
    return sprite;
  }
};
